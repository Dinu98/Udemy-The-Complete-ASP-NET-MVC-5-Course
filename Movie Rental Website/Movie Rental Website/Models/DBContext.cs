﻿using Movie_Rental_Website.Models;
using System.Data.Entity;

namespace DBcontext.Models

{

    public class DBcontext : DbContext

    {

        public DBcontext()

        {

        }

        //Entities

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Movie> Movies { get; set; }

    }

}