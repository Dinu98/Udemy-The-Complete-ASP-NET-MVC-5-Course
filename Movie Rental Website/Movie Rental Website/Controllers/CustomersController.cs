﻿using Movie_Rental_Website.Models;
using Movie_Rental_Website.ViewModels;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Movie_Rental_Website.Controllers
{
    public class CustomersController : Controller
    {
        public List<Customer> customers = new List<Customer>
            {
                new Customer {Name = "Customer1", Id = 1},
                new Customer {Name = "Customer2", Id = 2}
            };

        [Route("customers")]
        public ActionResult Index()
        {
            var viewModel = new RandomMovieViewModel
            {
                Customers = customers
            };

            return View(viewModel);
        }
        
        public ActionResult Edit(int id)
        {
            var customer = customers.Find(c => c.Id == id);

            if(customer != null)
            {
                return View(customer);
            }
            else
            {
                return HttpNotFound();
            }
        }
    }
}